//this file is...

//
//modules
//
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
//var morgan = require('morgan');
var app = express();
var routes = require("./app/routes");
//
//configuration steps
//

app.use(bodyParser.json());                                     
app.use(bodyParser.urlencoded({extended: true}));               
app.use(bodyParser.text());                                    
app.use(bodyParser.json({ type: 'application/json'}));  

routes(app);

//config files
var config = require('./config/config');


//Connecting MongoDB using mongoose to our application
var options = {
    server: {
        socketOptions: {
            keepAlive: 1,
            connectTimeoutMS: 30000
        }
    },
    replset: {
        socketOptions: {
            keepAlive: 1,
            connectTimeoutMS: 30000
        }
    }
};

mongoose.connect(config.db, options);

//This callback will be triggered once the connection is successfully established to MongoDB
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + config.db);
});

//Express application will listen to port mentioned in our configuration
app.listen(config.port, function (err) {
    if (err) throw err;
    console.log("App listening on port " + config.port);
});

module.exports = app;
