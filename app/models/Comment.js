var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//
//create the schema for comments 
//
CommentSchema = Schema({
    resourceId: {
        type: String,
        optional:false
    },
    comment:{
        type:String,
        optional:false
    }
    , userId: {
        type: String
        , optional: false
    }

});

//
//export the model
//

var CommentModel = mongoose.model('Comment', CommentSchema);

module.exports = CommentModel;