var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//
//define the schema for Users
//

UserSchema = Schema({
    email: {
        type: String
        , optional: false
    }
    , userId: {
        type: String
        , regEx: SimpleSchema.RegEx.Id
        , optional: false
    }
    , name: {
        type: String
        , optional: false
    }
   
});

//
//export the model
//
var UserModel = mongoose.model('User', UserSchema);
module.exports = UserModel;