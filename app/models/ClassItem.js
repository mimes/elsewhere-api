var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//
//define the schema for class items.
//
ClassItemSchema = Schema({
    className: {
        type: String,
        required:true,
        index:{unique:true}
    }
    , classDescription: {
        type: String
        , required:true
    },
    createdBy:{
        type: String,
        required:true
    }
});

//
//export the model
//
var ClassItemModel = mongoose.model('ClassItem', ClassItemSchema);

module.exports = ClassItemModel;