"use strict";
let mongoose = require('mongoose');
let Topic = require('../Topic');
let TopicDao = {
    getTopics: function (req, res) {
        Topic.find(function (error, topics) {
            //if there's an eror, send that. otherwise send the result in json format
            if (error) {
                res.send(error);
            } else {
                res.json(topics);
            }
        });
    }, //handles the operation for getting a single topic
    getTopic: function (req, res) {
        Topic.findById({
            _id: req.body._id
        }, function (err, topic) {
            if (!err) {
                res.json(topic);
            } else if (err) {
                res.json(err);
            }
        });
    }, //handles the PUT of topic
    updateTopic: function (req, res) {
        //first find the object that we should be looking for
        Topic.findById({
            _id: mongoose.Types.ObjectId(req.params.topic_id)
        }, function (err, topic) {
            if (err) {
                res.send(err)
            } else {
                Object.assign(topic, req.body).save((err, topic) => {
                    if (err) res.send(err);
                    res.json({
                        message: 'Topic updated!',
                        topic: topic
                    });
                });
            }
        });
    }, //method handles the DELETE operation
    deleteTopic: function (req, res) {
        //use the models remove function to remove the specified topic
        Topic.remove({
            _id: req.params.topic_id
        }, function (removeErr, removeResult) {
            if (!removeErr) {
                //console.log("removed " + removeResult + " topics.");
                res.json(removeResult);
            } else {
                console.log("error deleting a topic: " + removeErr.message);
                res.json(removeErr);
            }
        });
    }, //this method handles the posting of a topic
    postTopic: function (request, result) {
        let topic = new Topic(request.body);
        topic.save(function (error) {
            if (error) {
                console.log("unable to save Topic.  error message:  " + error.message);
                result.send(error);
            } else {
                result.json({
                    item: topic
                });
            }
        });
    }
}

module.exports = TopicDao;
