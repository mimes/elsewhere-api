"use strict";

let mongoose = require('mongoose');
let ClassItem = require('../ClassItem');

/*
this class dao object has all of the CRUD handlers for http requests for classes
*/
let ClassDao = {

    //this function handles the http GET requests for class items
    getClasses: function (req, res) {
        //use mongoose to get all classes
        ClassItem.find(function (err, classes) {
            //if error, then send the error
            if (err) {
                //response.send error
                res.send(err.message);
            } else {
                //return ClassItem results in JSON format
                res.json(classes);
            }
        })
    },

    //this function handles th http  post for classitems
    postClass: function (req, res) {
        var propValue;
        //console.log("req.body:");
        //console.log(req.body);
        let classItem = new ClassItem(req.body);


        classItem.save(function (error, classItem, numAffected) {

            if (error) {

                console.log("unable to save classitem.  error message:  " + error.message);
                console.log(error);
                res.send(error);
            } else {
                res.json({
                    item: classItem
                });
            }

        });
    },

    //this method handles http GET requests for getting classes 
    getClass: function (req, res) {
        var classId = mongoose.Types.ObjectId(req.params.class_id);
        console.log("get classid: " + classId);
        ClassItem.find({
            _id: classId
        }, function (err, classItem) {
            if (!err) {
                res.json(classItem);
            } else if (err) {
                res.json(err);
            }
        });
    },

    //this method handles http PUT requests for updating classes
    updateClass: function (req, res) {
        console.log("put route id given\n" + req.params.class_id);
        ClassItem.findById({
            _id: mongoose.Types.ObjectId(req.params.class_id)
        }, function (err, classItem) {
            //console.log(classItem);
            if (err) {
                console.log(err);
                res.send(err)
            } else {
                Object.assign(classItem, req.body).save((err, topic) => {

                    if (err) {
                        res.send(err);
                    } else {
                        res.json({
                            message: 'ClassItem updated!',
                            classItem: classItem
                        });
                    }
                });
            }
        });
    }
}
module.exports = ClassDao;
