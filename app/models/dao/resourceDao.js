"use strict";
let mongoose = require('mongoose');
let Resource = require('../Resource');
/*
this dao object contains HTTP request handlers for Resources
*/
let ResourceDao = {
    //this method handles GET http requests for getting resources
    getResources: function (req, res) {
        Resource.find(function (foundError, foundClasses) {
            //if an error occured, send that
            if (foundError) {
                res.json(foundError);
            } else {
                //otherwise, just send the results
                res.json(foundClasses);
            }
        });
    }, //this method handles POST HTTP requests for creating resources
    postResource: function (req, res) {
            let resource = new Resource(req.body);
            resource.save(function (saveError, savedResource, numAffected) {
                //confirm that one change was made and there was no error before
                //sending success result
                if (numAffected === 1 && !saveError) {
                    res.json(savedResource);
                } else {
                    res.json({
                        error: saveError
                    });
                }
            });
        }, //this method hanldes PUT HTTP requests for updating resources

    
    updateResource: function (req, res) {
            let resourceId = mongoose.Types.ObjectId(req.params.resource_id);
            Resource.findById({
                _id: resourceId
            }, function (error, resource) {
                if (err) {
                    console.log(err);
                    res.send(err)
                } else {
                    Object.assign(classItem, req.body).save((err, topic) => {
                        if (err) {
                            res.send(err);
                        } else {
                            res.json({
                                message: 'ClassItem updated!',
                                classItem: classItem
                            });
                        }
                    });
                }
            });
        }, //this method hanldes deleting agiven resource by it's id
        
    deleteResourceById: function (req, res) {

        //get the id and cast it to a mongoose objectId object
        let resourceId = mongoose.Types.ObjectId(req.params.resource_id);

        Resource.remove({
            _id: resourceId
        }, function (removeError, removeResult) {
            if (!removeErr) {
                res.json(removeResult);
            } else {
                console.log("error deleting a resource: " + removeError.message);
                res.json(removeError);
            }
        });
    }, //this method handles deleting a resource given any number of parameters
    deleteResource: function (request, result) {
        let match = Resource.find(
            request.body,
            function (findError, foundResources) {

                //if found resources is 0 or less, then do nothing
                if (foundResources.length > 0) {
                    //if 
                } else {
                    //nothing to delete
                }
            })
    }
}

//export the resource dao object
module.exports = ResourceDao;
