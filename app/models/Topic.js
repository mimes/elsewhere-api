var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//
//define the schema for topics
//

TopicSchema = Schema({
    topicDescription: {
        type: String,
        required: true
    }
    , classId: {
        type: String,
        required:true
    }
    , topicName: {
        type: String,
        required: true,
        index:{unique:true}
    },
    createdBy: {
        type: String,
        required: true
    }
});

//
//export the model
//

var TopicModel = mongoose.model('Topic', TopicSchema);
module.exports = TopicModel;