var mongoose = require("mongoose");
var Schema = mongoose.Schema;

//
//create the schema
//
RatingSchema = Schema({
    resourceId: {
        type: String,
        optional:false
    },
    userId:{
        type:String,
        optional:false
    },
    rating:{
        type: Number,
        optional:false
    }
});

//
//export the model
//

var RatingModel = mongoose.model('Rating', RatingSchema);

module.exports = RatingModel;