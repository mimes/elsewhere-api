"use strict";

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

//
//define the schema for resources
//
let ResourceSchema = Schema({
    topicName: {
        type: String,
        optional: false
    },
    topicId: {
        type: String,
        optional: false
    },
    resourceDescription: {
        type: String,
        optional: false
    },
    resourceContentId: {
        type: String
    },
    userId: {
        type: String,
        optional: false
    }
});

let ResourceModel = mongoose.model('Resource', ResourceSchema);

module.exports = ResourceModel;
