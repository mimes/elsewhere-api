var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//
//define the subscribed class schema
//
SubscribedClassSchema = Schema({
    classId: {
        type: String,
        required: true
    },
    className: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    }
});


//make sure that userId and classId is a unique 1 to 1 pairing
SubscribedClassSchema.index({userId: 1, classId: 1});

//create the model from the schema
var SubscribedClassModel = mongoose.model('SubscribedClass', SubscribedClassSchema);

//export the model
module.exports = SubscribedClassModel;
