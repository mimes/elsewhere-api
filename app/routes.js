//load the  models
var ClassItem = require('./models/ClassItem');
var Topic = require("./models/Topic");
var mongoose = require("mongoose");
var TopicDao = require("./models/dao/topicDao");
var ClassDao = require("./models/dao/classDao");
var ResourceDao = require("./models/dao/resourceDao")

//expose the server routes we're about to create
module.exports = function (app) {

    //
    //server routes
    //
    app.route("/classitem")
        //get class items
        .get(ClassDao.getClasses)
        //create a class item
        .post(ClassDao.postClass);

    app.route("/classitem/:class_id")

    //get a single class item
    .get(ClassDao.getClass)

    //modify a class item with a given id
    .put(ClassDao.updateClass);

    app.route("/topic")
        .get(TopicDao.getTopics)
        .post(TopicDao.postTopic);

    app.route("/topic/:topic_id")
        .get(TopicDao.getTopic)
        .put(TopicDao.updateTopic)

    .delete(TopicDao.deleteTopic);


    //
    //front end routes
    //

    //route to hanlde all angular requests
    app.get('*', function (req, res) {
        //load the main page of the application
        //res.sendfile('./public/views/index.html');
    });
}
