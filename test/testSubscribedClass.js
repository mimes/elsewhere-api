//connect to the database
var mongoose = require('mongoose');
var config = require('../config/config');
mongoose.connect(config.db);

//get the model we want to test
SubscribedClass = require("../app/models/SubscribedClass");

//include the expect api from chai
var expect = require('chai').expect;



//
//unit testing for class item
//




describe("SubscribedClass model", function () {

    //unit testing variables
    var subscribedClassData = {};

    //define any necessary setup before running a test
    before(function () {
        subscribedClassData.simple = {
            classId: "someClassId",
            className: "Prog Lang",
            userId: "backus normal form"
        };
    });

    beforeEach(function (done) {
        //empty the database of SubscribedClasss
        SubscribedClass.remove({}, function () {
            done();
        });

    });

    it("the database should be empty when tests start", function (done) {
        SubscribedClass.find(function (error, results) {
            if (error) {
                done(error);
            }
            expect(results.length).to.be.equal(0);
            done();
        })
    });

    it("should have all required fields", function (done) {
        var topic = new SubscribedClass();

        topic.validate(function (error) {
            expect(error).to.exist;

        });

        subscribedClass = new SubscribedClass(subscribedClassData.simple);
        subscribedClass.validate(function (error) {
            expect(error).to.not.exist;
            done();
        });

    });

    it("should save without error when all required fields are there", function (done) {

        var subscribedClass = new SubscribedClass(subscribedClassData.simple);
        subscribedClass.save(function (error, product, numAffected) {
            expect(numAffected).to.equal(1);
            if (!error) {
                done();
            } else {
                done(error);
            }
        });


    });

    it("should not allow duplicate names", function (done) {
        var subscribedClass = new SubscribedClass(subscribedClassData.simple);
        subscribedClass.save(function (error) {
            expect(error).to.not.exist;
        });

        subscribedClass2 = new SubscribedClass(subscribedClassData.simple);
        subscribedClass2.save(function (error, product, numAffected) {
            expect(error).to.exist;
            done();
        });

    });
});
