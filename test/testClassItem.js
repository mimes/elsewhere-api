    "use strict";
    //
    //Require the dev-dependencies
    //
    let Q = require('q');
    let chai = require('chai');
    let chaiHttp = require('chai-http');
    //include the expect api from chai
    let expect = require('chai').expect;
    chai.use(chaiHttp);
    let mongoose = require('mongoose');
    //connect to the database
    //let mongoose = require('mongoose');
    let config = require('../config/config');
    //mongoose.connect(config.db);
    //get the model we want to test
    let ClassItem = require("../app/models/ClassItem");
    //get the express server
    let server = require('../server');
    //set mongoose.Promise use the q promise library
    mongoose.Promise = require('q').Promise;
    //
    //unit testing for class item
    //
    describe("ClassItem", function () {
        //unit testing letiables
        let classData = {};
        //define any necessary setup before running any tests
        before(function () {
            classData.simple = {
                className: "Programming Languages",
                classDescription: "a class about proglang",
                createdBy: "someUserID"
            };
            classData.calc = {
                className: "calculus",
                classDescription: "a class about calculus",
                createdBy: "someuserId"
            };
        });
        //before each test, clear the database
        beforeEach(function (done) {
            //empty the database of classitems
            ClassItem.remove({}, function () {
                done();
            });
        });
        describe("ClassItem model", function () {
            //before each test, clear the database
            beforeEach(function (done) {
                //empty the database of classitems
                ClassItem.remove({}, function () {
                    done();
                });
            });
            it("the database should be empty when tests start", function (done) {
                ClassItem.find(function (error, results) {
                    if (error) {
                        done(error);
                        //console.log("error getting empty result: " + error);
                    }
                    expect(results.length).to.be.equal(0);
                    done();
                })
            });
            /*        it("should not save without all required fields", function (done) {
                        let classItem = new ClassItem({});

                        classItem.validate(function (error) {
                            console.log("error in save without req: " + error);
                            expect(error.errors).to.exist;
                            done();
                        });

                    });*/
            it("should save without error when all required fields are there", function (done) {
                let classItem = new ClassItem(classData.simple);
                classItem.save(function (error, product, numAffected) {
                    expect(numAffected).to.equal(1);
                    if (!error) {
                        done();
                    } else {
                        done(error);
                    }
                });
            });
            it("should not allow duplicate names", function (done) {
                let classItem = new ClassItem(classData.simple);
                let classItem2 = new ClassItem(classData.simple);
                //save two class items with the same name, and make sure the second one fails 
                classItem.save(function (error, product, numAffected) {
                    expect(error).to.not.exist;
                    expect(numAffected).to.equal(1);
                    classItem2.save(function (error, product) {
                        expect(error).to.exist;
                        done();
                    });
                })
            });
            it("should delete all references to a ClassItem record in subscribed classes if a record is deleted");
        });
        describe("ClassItem API", function () {
            //before each test, clear the database
            beforeEach(function (done) {
                //empty the database of classitems
                ClassItem.remove({}, function () {
                    done();
                });
            });
            it("should make books using POSt", function (done) {
                chai.request(server).post("/classitem").send(classData.simple).end(function (req, res) {
                    expect(true).to.equal(true);
                    done();
                });
            });
            /*then(function(){})
                            chai.request(server)
                                .post('/classitem')
                                .send(classData.calc)
k


                            .end(function (err, res) {
                                if (err) {
                                    done(err);
                                } else {
                                    //console.log(res);
                                    //console.log(res.status)
                                    expect(err).to.be.null;
                                    expect(res).to.have.status(200);
                                    //console.log(res);
                                    done();
                                }

                            });*/
            it("should get all off the classitems using GET and /classitem", function (done) {
                //add a class
                chai.request(server).post('/classitem').send(classData.simple).end(function (err, res) {
                    //console.log(res);
                    //console.log(res.status)
                    expect(res.status).to.equal(200);
                    expect(res.body).to.be.a('object');
                });
                //get all the classes
                chai.request(server).get('/classitem').end((err, res) => {

                    //make sure that the res object has the correct answers
                    expect(res.status).to.equal(200);
                    expect(res.body).to.be.a('array');
                    expect(res.body.length).to.be.equal(1);
                    done();
                });
            });
            it("should get a single class using GET and an id", function (done) {
                let classId = "";

                //use of promise here extraneous--wanted to use Q.defer() in a reasonable example.
                //could have used classitem.save(...) chaining
                var requestPromise = Q.defer();
                chai.request(server)
                    .post('/classitem')
                    .send(classData.calc)
                    .end(function (err, res) {

                        try {
                            //save the class id for the second part of the test
                            classId = res.body.item._id;

                            //make sure the error doesn't exist and the res object has what we want
                            expect(err).to.not.exist;
                            expect(res.status).to.equal(200);
                            expect(res.body).to.be.a('object');
                            requestPromise.resolve();

                        } catch (e) {
                            console.log("error: " + e.message);
                            requestPromise.promise.reject("Unable to get the classId from req.body " + e.message);
                        }
                    })

                //after the promise is resolved, make the server request
                requestPromise.promise.then(function () { //use the /classitem/:class_id route to try to get the newly created item

                    chai.request(server).get("/classitem/" + classId)
                        .end(function (error, res) {

                            //we shouldn't have errors, and the status should be correct
                            expect(res.status).to.equal(200);
                            expect(error).to.not.exist;
                            expect(res).to.exist;
                            expect(res).to.be.a('object');

                            //it should only return one thing, so we'll check if the result has the right id
                            expect(res.body[0]._id).to.equal(classId);
                            done();
                        });
                }, function (rejectReason) {
                    //if the promise is rejected, log the reason for debugging purposes

                    console.log(rejectReason);
                    done(rejectReason);
                });
            });

            it("should modify a given classitem with PUT", function (done) {
                let classItem = new ClassItem(classData.calc);

                classItem.save(function (err, newClassItem, numAffected) {

                    chai.request(server)
                        .put("/classitem/" + classItem._id)
                        .send({
                            classDescription: "new",
                            className: classData.simple.className,
                            createdBy: classData.simple.createdBy
                        })
                        .end(function (err, res) {
                            expect(res.status).to.equal(200);
                            expect(err).to.not.exist;
                            expect(res.body.message).to.exist;
                            console.log(res.body);
                            expect(res.body.classItem.classDescription).to.equal("new");
                            done();
                        });
                })
            });
        });
    });
