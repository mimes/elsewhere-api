    "use strict";
    //
    //Require the dev-dependencies
    //
    let chai = require('chai');
    let chaiHttp = require('chai-http');
    //include the expect api from chai
    let expect = require('chai').expect;
    chai.use(chaiHttp);


    //connect to the database
    //let mongoose = require('mongoose');
    let config = require('../config/config');
    //mongoose.connect(config.db);

    //get the model we want to test
    let Topic = require("../app/models/Topic");

    //get the express server
    let server = require('../server');


    //

    //
    //unit testing for class item
    //




    describe("Topic", function () {

        //unit testing variables
        var topicData = {};

        //define any necessary setup before running a test
        before(function () {
            topicData.simple = {
                topicName: "BNFasdfasd",
                classId: "someClassId",
                topicDescription: "backus normal form",
                createdBy: "someUserId"
            };
        });

        //empty the database after the test run is complete
        after(function (done) {
            Topic.remove({}, function (error) {
                if (error) {
                    done(error);
                } else {
                    done();
                }

            })
        });
        describe("Topic model", function () {

            beforeEach(function (done) {
                //empty the database of Topics
                Topic.remove({}, function () {
                    done();
                });

            });

            it("the database should be empty when tests start", function (done) {
                Topic.find(function (error, results) {
                    if (error) {
                        done(error);
                    }
                    expect(results.length).to.be.equal(0);
                    done();
                })
            });

            it("should have all required fields", function (done) {
                var topic = new Topic();

                topic.validate(function (error) {
                    expect(error).to.exist;

                });

                topic = new Topic(topicData.simple);
                topic.validate(function (error) {
                    expect(error).to.not.exist;
                    done();
                });

            });

            it("should save without error when all required fields are there", function (done) {

                var topic = new Topic(topicData.simple);
                topic.save(function (error, product, numAffected) {
                    expect(numAffected).to.equal(1);
                    if (!error) {
                        done();
                    } else {
                        done(error);
                    }
                });


            });

            it("should not allow duplicate names", function (done) {
                var topic = new Topic(topicData.simple);
                topic.save(function (error) {
                    expect(error).to.not.exist;

                    let topic2 = new Topic(topicData.simple);
                    topic2.save(function (error, product, numAffected) {
                        expect(error).to.exist;
                        done();
                    });
                });
            });
        });

        describe("Topic api", function () {

            beforeEach(function (done) {
                //empty the database of Topics
                Topic.remove({}, function () {
                    done();
                });

            });

            it("should get all topics with get and /topic ", function (done) {
                chai.request(server)
                    .get('/topic')
                    .end(function (error, result) {
                        if (error) {
                            done(error);
                        }
                        expect(result.status).to.equal(200);
                        expect(result.body).to.be.a('array');
                        done();
                    });
            });
            it("should create a topic with POST and /topic", function (done) {
                console.log(topicData.simple);
                chai.request(server)
                    .post('/topic')
                    .send(topicData.simple)
                    .end(function (error, result) {

                        //if we hit an error, pass the error to the done callback
                        if (error) {
                            done(error);
                        } else {
                            expect(result.status).to.equal(200);
                            expect(result.body).to.be.a('object');
                            done();
                        }
                    });
            });
            it("should update a topic with PUT and /topic", function (done) {

                //create a topic first
                let topic = new Topic(topicData.simple);
                topic.save(function (error, product, numAffected) {
                    expect(error).to.not.exist;
                    if (!error) {
                        //console.log(Topic.find({}));
                        //make the put request
                        chai.request(server)
                            .put("/topic/" + topic._id)
                            .send({
                                topicDescription: "new",
                                topicName: topicData.simple.topicName,
                                createdBy: topicData.simple.createdBy,
                                classId: topicData.simple.classId
                            })
                            .end(function (err, res) {
                                expect(res.status).to.equal(200);
                                expect(res.body.topic).to.exist;
                                expect(err).to.not.exist;
                                done();
                            });
                    } else {
                        done(error);
                    }
                });

            });
            it("should delete a topic with /topic:topicId", function (done) {
                let topic = new Topic(topicData.simple);
                topic.save(function (saveError, saveProduct, numAffected) {
                    if (saveError) {
                        console.log("error saving topic to delete " + saveError);
                    } else {
                        chai.request(server)
                            .delete("/topic/" + topic._id)
                            .end(function (endError, endResult) {
                                Topic.find({}, function (findError, topics) {

                                    //there won't be an error on successful deletion
                                    expect(findError).to.not.exist;

                                    //on successful deletion there should be 0 topics given back by find
                                    expect(topics.length).to.equal(0);
                                    done();
                                });
                            });
                    }
                });

            });
            it("should get a given topic with /topic:topicId");
        });
    });
